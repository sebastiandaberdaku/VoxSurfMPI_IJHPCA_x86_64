/*
 * Pocket.cpp
 *
 *  Created on: Sep 24, 2015
 *      Author: sebastian
 */

#include "Pocket.h"
#include <queue>
/**
 * Constructor of the class.
 */
Pocket::Pocket() : isPocket(false) {}
Pocket::Pocket(Pocket const & p) :
		isPocket(p.isPocket), SA_left(p.SA_left), SA_right(p.SA_right),
		pocketVoxels(p.pocketVoxels), leftBorder(p.leftBorder), rightBorder(p.rightBorder) { }

Pocket::Pocket(voxelGrid const & pocketSurface, voxelGrid const & cpkModel) : isPocket(false) {
	voxelGrid l_border2D(1, pocketSurface.width, pocketSurface.height);
	voxelGrid r_border2D(1, pocketSurface.width, pocketSurface.height);

	for (int i = 0; i < pocketSurface.length; ++i) {
		for (int j = 0; j < pocketSurface.width; ++j) {
			for (int k = 0; k < pocketSurface.height; ++k) {
				if (pocketSurface.getVoxel(i, j, k)) {
					pocketVoxels.push_back(voxel(i, j, k));
					if (i == 0) {
						border_voxel current(j, k);
						for (int ii = 0; ii < 8; ++ii) {
							border_voxel nb = current + border_offset::neighbours[ii];
							if (!cpkModel.getVoxel(0, nb.iy, nb.iz)) {
								l_border2D.setVoxel(0, j, k);
								break;
							}
						}
					}
					if (i == pocketSurface.length - 1) {
						border_voxel current(j, k);
						for (int ii = 0; ii < 8; ++ii) {
							border_voxel nb = current + border_offset::neighbours[ii];
							if (!cpkModel.getVoxel(pocketSurface.length - 1, nb.iy, nb.iz)) {
								r_border2D.setVoxel(0, j, k);
								break;
							}
						}
					}
				}
			}
		}
	}
	for (int j = 0; j < pocketSurface.width; ++j) {
		for (int k = 0; k < pocketSurface.height; ++k) {
			if (l_border2D.getVoxel(0, j, k)) {
				vector<border_voxel> c_border;
				std::queue<border_voxel> l_queue;
				border_voxel start(j, k);
				l_queue.push(start);
				l_border2D.clearVoxel(0, j, k);
				for (int ii = 0; ii < 4; ++ii) {
					voxel nbx = voxel(0, j, k) + voxel_offset::neighbours[ii];
					if (!cpkModel.getVoxel(nbx)) {
						c_border.push_back(start);
						break;
					}
				}
				while (!l_queue.empty()) {
					border_voxel current = l_queue.front();
					l_queue.pop();
					for (int ii = 0; ii < 8; ++ii) {
						border_voxel nb = current + border_offset::neighbours[ii];
						if (l_border2D.getVoxel(0, nb.iy, nb.iz)) {
							for (int jj = 0; jj < 4; ++jj) {
								voxel nbx = voxel(0, nb.iy, nb.iz) + voxel_offset::neighbours[jj];
								if (!cpkModel.getVoxel(nbx)) {
									c_border.push_back(nb);
									break;
								}
							}
							l_queue.push(nb);
							l_border2D.clearVoxel(0, nb.iy, nb.iz);
						}
					}
				} // while
				if (!c_border.empty())
					leftBorder.push_back(c_border);
			}//if

			if (r_border2D.getVoxel(0, j, k)) {
				vector<border_voxel> c_border;
				std::queue<border_voxel> r_queue;
				border_voxel start(j, k);
				r_queue.push(start);
				r_border2D.clearVoxel(0, j, k);
				for (int ii = 0; ii < 4; ++ii) {
					voxel nbx = voxel(pocketSurface.length - 1, j, k) + voxel_offset::neighbours[ii];
					if (!cpkModel.getVoxel(nbx)) {
						c_border.push_back(start);
						break;
					}
				}
				while (!r_queue.empty()) {
					border_voxel current = r_queue.front();
					r_queue.pop();
					for (int ii = 0; ii < 8; ++ii) {
						border_voxel nb = current + border_offset::neighbours[ii];
						if (r_border2D.getVoxel(0, nb.iy, nb.iz)) {
							for (int jj = 0; jj < 4; ++jj) {
								voxel nbx = voxel(pocketSurface.length - 1, nb.iy, nb.iz) + voxel_offset::neighbours[jj];
								if (!cpkModel.getVoxel(nbx)) {
									c_border.push_back(nb);
									break;
								}
							}
							r_queue.push(nb);
							r_border2D.clearVoxel(0, nb.iy, nb.iz);
						}
					}
				} // while
				if (!c_border.empty())
					rightBorder.push_back(c_border);
			}//if
		}
	}
	SA_left.resize(leftBorder.size(), false);
	SA_right.resize(rightBorder.size(), false);
}
Pocket & Pocket::operator=(Pocket const & p) {
	if (this != &p) {// protect against invalid self-assignment
		this->isPocket = p.isPocket;
		this->SA_left = p.SA_left;
		this->SA_right = p.SA_right;
		this->leftBorder = p.leftBorder;
		this->rightBorder = p.rightBorder;
		this->pocketVoxels = p.pocketVoxels;
	}
    // by convention, always return *this
	return *this;
}

bool Pocket::hasLeftBorder() const {
	return !leftBorder.empty();
}
bool Pocket::hasRightBorder() const {
	return !rightBorder.empty();
}
