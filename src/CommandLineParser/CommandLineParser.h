/*
 * CommandLineParser.h
 *
 *  Created on: Sep 6, 2014
 *      Author: sebastian
 */

#ifndef COMMANDLINEPARSER_H_
#define COMMANDLINEPARSER_H_

#include <string>

using namespace std;

/**
 * This class provides a simple interface for accessing the command line arguments.
 */
class CommandLineParser {
public:
	static bool parseCommandLine(int const & argc, char const * const argv[],
			int & surfaceType, float & probeRadius, float & resolution,
			string & inname, string & outname, bool & help,
			bool & version, bool & surf_description, bool & license);
};

#endif /* COMMANDLINEPARSER_H_ */
