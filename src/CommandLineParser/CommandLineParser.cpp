/*
 * CommandLineParser.cpp
 *
 *  Created on: Sep 6, 2014
 *      Author: sebastian
 */

#include "../utils/disclaimer.h"
#include "CommandLineParser.h"
#include "CustomValidators.h"

#include <mpi/mpi.h>
/**
 * This method parses the input command line options.
 * @param argc		input argc reference, i.e. the first argument of the main method
 * @param argv		input argv reference, i.e. the second argument of the main method
 *
 * @return 			true if all arguments are parsed correctly, false otherwise.
 */
bool CommandLineParser::parseCommandLine(int const & argc, char const * const argv[],
				int & surfaceType, float & probeRadius, float & resolution,
				string & inname, string & outname, bool & help,
				bool & version, bool & surf_description, bool & license) {

	//------------------------------- command line options --------------------------------------
	options_description description("VoxSurfMPI - Voxelized protein surface calculation program based on MPI. Usage");
	description.add_options()("help,h", "Display this brief help message.")
	/*
	 * This token is used to specify the input filename. It is mandatory.
	 * The pqr command line token has no option name. The command line tokens which have
	 * no option name are called "positional options" in the boost::program_options library.
	 */
	("pdb", value<input_PDB_filename>()->required(), "Input PDB file.")
	/*
	 * This token is used to specify the output filename.
	 */
	("output,o", value<output_filename>(), "Output filename. If not specified, the input filename (with no extension) will be used.")
	/*
	 * The --surf_description flag is used to give a brief description of the supported surface types.
	 */
	("surf_description", "Prints a brief description of the supported surface types.")
	/*
	 * This token is used to specify the surface type.
	 */
	("surf_type,s", value<surface_type>()->default_value(3), "Surface type, 1-van der Waals 2-Solvent Accessible 3-Solvent Excluded (default is 3).")
	/*
	 * The -p (--probe_radius) flag is used to specify the probe radius. Default is 1.4. Optional.
	 */
	("probe_radius,p", value<probe_radius>()->default_value(1.4), "Probe radius (in Å), floating point number in (0, 2.0] (default is 1.4Å).")
	/*
	 * The -r (--resolution) flag is used to specify the resolution of the voxel grid. Default is 4. Optional.
	 */
	("resolution,r", value<resolution_param>()->default_value(4.0), "Resolution factor, positive floating point (default is 4.0). This value's cube determines the number of voxels per Å³.")
	/*
	 * The -l (--license) flag is used to view the program's license
	 * information.
	 */
	("license,l", "View license information.")
	/*
	 * The -v (--version) flag is used to view the program's version
	 * information.
	 */
	("version,v", "Display the version number");
	/*
	 * The input filename must be declared as positional.
	 */
	positional_options_description p;
	p.add("pdb", 1);

	variables_map vm;
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	try {
		//--------------------------------parsing command line options------------------------------
		/*
		 * And it is finally specified when parsing the command line.
		 */
		store(command_line_parser(argc, argv).options(description).positional(p).run(), vm);

		if (vm.count("help")) {
			if (!rank)
				cout << description;
			help = true;
			return true;
		}
		if (vm.count("version")) {
			if (!rank)
				cout << "Program: " << argv[0] << ", version: " << PROGRAM_VERSION << "\n";
			version = true;
			return true;
		}
		if (vm.count("surf_description")) {
			if (!rank)
				cout << PROGRAM_NAME << "\nThe program calculates the following surfaces:\n" << VWS << SAS << MS;
			surf_description = true;
			return true;
		}
		if (vm.count("license")) {
			if (!rank)
				DISCLAIMER
			license = true;
			return true;
		}
		/*
		 * notify throws exceptions so we call it after the above checks
		 */
		notify(vm);

		/* initializing variables */

		inname = vm["pdb"].as<input_PDB_filename>().filename;
		if (vm.count("output")) {
			outname = vm["output"].as<output_filename>().outname;
		} else {
			int lastindex = inname.find_last_of(".");
			outname = inname.substr(0, lastindex);
		}
		surfaceType = vm["surf_type"].as<surface_type>().st;
		probeRadius = vm["probe_radius"].as<probe_radius>().p;
		resolution = vm["resolution"].as<resolution_param>().r;

	} catch (error const & e) {
		if (!rank) {
			cerr << "error: " << e.what() << "\n";
			cerr << description << "\n";
		}
		return false;
	}
	return true;
};


