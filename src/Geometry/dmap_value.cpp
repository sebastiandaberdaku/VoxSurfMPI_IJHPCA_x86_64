/*
 * dmap_value.cpp
 *
 *  Created on: Sep 26, 2015
 *      Author: sebastian
 */

#include "dmap_value.h"


dmap_value::dmap_value() :
	ix(-1), iy(-1), iz(-1), d(-1), nx(-1), ny(-1), nz(-1) { }

dmap_value::dmap_value(dmap_value const & dmv) :
	ix(dmv.ix), iy(dmv.iy), iz(dmv.iz), d(dmv.d), nx(dmv.nx), ny(dmv.ny), nz(dmv.nz) { }

dmap_value::dmap_value(int ix, int iy, int iz, int d, int nx, int ny, int nz) :
	ix(ix), iy(iy), iz(iz), d(d), nx(nx), ny(ny), nz(nz) { }

dmap_value & dmap_value::operator=(dmap_value const & dmv) {
	if (this != &dmv) {
		this->ix = dmv.ix;
		this->iy = dmv.iy;
		this->iz = dmv.iz;
		this->d = dmv.d;
		this->nx = dmv.nx;
		this->ny = dmv.ny;
		this->nz = dmv.nz;
	}
	return *this;
}
